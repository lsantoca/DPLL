module Interactions (waitWithMessage) where

waitWithMessage :: String -> IO ()
waitWithMessage str =
  putStr str >> getChar >>= \_ -> return ()

