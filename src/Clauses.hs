module Clauses(
  Variable(..),Literal(..),Clause(..),Clauses(..),
  Valuation,addPair,emptyValuation,
  vars,opposite,len,containsEmpty,contains,delete,setLitTrue,setLitFalse,
  parseFileDIMACS
  ) where

import Data.List (intercalate,nub)
import Data.List.Split (endByOneOf)
import Data.Maybe (fromJust)

data Variable = Var Int deriving (Eq,Ord)
parseVariableDIMACS :: String -> Maybe Variable
parseVariableDIMACS str = Just (Var ((read str)::Int))

data Literal = Positive Variable | Negative Variable deriving Eq
opposite :: Literal -> Literal
opposite (Positive p) = Negative p
opposite (Negative p) = Positive p

parseLiteralDIMACS :: String -> Maybe Literal
parseLiteralDIMACS string = 
  case head string of
    '-' -> parseVariableDIMACS (tail string) >>= \variable -> Just (Negative (variable))
    _ -> parseVariableDIMACS string >>= \variable -> Just (Positive (variable))

parseClauseDIMACS :: String -> Maybe Clause
parseClauseDIMACS string =
  let
    varsString = [str | str <- endByOneOf " \t0" string, not (null str) ]
  in
    Just (Disjunction (map (fromJust . parseLiteralDIMACS) varsString))

parseClausesDIMACS :: String -> Maybe Clauses
parseClausesDIMACS string =
  let
    clausesListOfString = lines string
  in
    Just (SetOf (map (fromJust . parseClauseDIMACS) clausesListOfString))

parseFileDIMACS :: String -> IO Clauses
parseFileDIMACS fileName = do
  content <- readFile fileName
  ls <- return [line | line <- lines content, not (null line)]
  linesNoComments <- return [line | line <-ls, not $ elem (head line) "cp" ]
  return (SetOf (map (fromJust . parseClauseDIMACS) linesNoComments))

data Clause = Disjunction [Literal] deriving Eq
data Clauses = SetOf [Clause] deriving Eq

data Valuation = Valuation [(Variable,Bool)]
instance Show Valuation where
  show (Valuation pairs) =
    let
      strings = 
        map (\(x,y)-> show x++" -> "++show y) pairs
    in
      "{ "++intercalate ", " (reverse strings) ++ " }"

emptyValuation :: Valuation
emptyValuation = Valuation  []
addPair :: (Variable,Bool) -> Valuation -> Valuation
addPair (x,b) (Valuation pairs) = Valuation ((x,b):pairs)
  
instance Show Variable where
  show (Var n) = "p_"++show n

instance Show Literal where
  show (Positive v) = show v
  show (Negative v) = "~"++show v

instance Show Clause where
  show (Disjunction literals)
    | null literals = "false"
    | otherwise  = intercalate " v " (map show literals)

instance Show Clauses where
  show (SetOf clauses) = "[\n\t"++intercalate ",\n\t" (map show clauses) ++"\n]"

class Formula a where
  vars :: a -> [Variable]

instance Formula Literal where
  vars (Positive v) = [v]
  vars (Negative v) = [v]

instance Formula Clause where
  vars (Disjunction literals) = nub $ concat (map vars literals)

instance Formula Clauses where
  vars (SetOf clauses) = nub $ concat (map vars clauses)

len :: Clause -> Int
len (Disjunction literals) = length literals

containsEmpty :: Clauses -> Bool
containsEmpty (SetOf clauses) = elem (Disjunction []) clauses

contains :: Clause -> Literal -> Bool
contains (Disjunction literals) literal = elem literal literals 

delete :: Literal -> Clause -> Clause
delete literal (Disjunction literals) =
  Disjunction [lit | lit <- literals, lit /= literal ]

setLitTrue :: Clauses -> Literal -> Clauses
setLitTrue (SetOf clauses) literal =
  let
    interestingClauses = 
      [ clause |  clause <- clauses, not (contains clause literal) ]
  in
    SetOf (map (delete (opposite literal)) interestingClauses)

setLitFalse :: Clauses -> Literal -> Clauses
setLitFalse clauses literal = setLitTrue clauses (opposite literal)
