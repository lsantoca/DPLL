module RandomClauses where

import System.Random
import Data.List (nub)

import Clauses

instance Random Variable where
  random g = case randomR (1,5) g of
    (n,g') -> (Var n,g')

instance Random Literal where
  random g =
    let
      (var,g') = random g
    in
      case randomR (1,2::Int) g' of
        (1,g'') -> (Positive var,g'')
        (2,g'') -> (Negative var,g'')

randomList 0 g = ([],g)
randomList n g =
  let
    (r,g') = random g
    (rs,g'') = randomList (n-1) g'
  in
    (r:rs,g'')

instance Random Clause where
  random g =
    let
      (n,g') = randomR (2,10::Int) g
      (literals,g'') = randomList n g'  
    in
      (Disjunction (nub literals),g'')

instance Random Clauses where
  random g =
    let
      (n,g') = randomR (20,40::Int) g
      (clauses,g'') = randomList n g'  
    in
      (SetOf (nub clauses),g'')
