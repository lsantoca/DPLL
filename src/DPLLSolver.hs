module DPLLSolver where

-- hide sortOn for ghc 7.6 compatibility
import Data.List hiding (sortOn)
import Data.Ord (comparing)
---
import Text.Printf
import Data.IORef
import System.IO.Unsafe

import Clauses
import Interactions

-- Flags
isInteractive :: IORef Bool
{-# NOINLINE isInteractive #-}
isInteractive = unsafePerformIO (newIORef False)

-- Rela variables
alpha :: IORef Float
{-# NOINLINE alpha #-}
alpha = unsafePerformIO (newIORef 0.7)
beta :: IORef Float
{-# NOINLINE beta #-}
beta = unsafePerformIO (newIORef 0.3)


type Ranking = [(Literal,Float)]

-- Rankings :
-- we need to have something on bewteen 0 and 1
rankMOMsLike :: Clauses -> Literal -> Float
rankMOMsLike (SetOf clauses) literal =
  let
    this =  sum [1/fromIntegral (len clause) | clause <- clauses, contains clause literal ]
    tot = sum [1/fromIntegral (len clause) | clause <- clauses ]
  in
    this/tot

rankDLISLike :: Clauses -> Literal -> Float
rankDLISLike (SetOf clauses) literal =
  let
    this = fromIntegral $
      sum [len clause | clause <- clauses, contains clause (opposite literal)]
    tot =  fromIntegral $ sum [len clause | clause <- clauses]
  in
    this/tot

rankMixedLike :: Clauses -> Literal -> Float
rankMixedLike clauses literal =
  let
    rankmoin = rankMOMsLike clauses literal 
    rankplus = rankDLISLike  clauses literal
    a = unsafePerformIO $ readIORef alpha
    b = 1- a
  in
    a * rankmoin + b * rankplus

-- sortOn is only available starting at ghc 7.10
sortOn :: Ord b => (a -> b) -> [a] -> [a]
sortOn f =
  map snd . sortBy (comparing fst) . map (\x -> let y = f x in y `seq` (y, x))

rankAll :: (Clauses -> Literal -> Float) -> Clauses -> Ranking
rankAll how clauses =
  let
    literals = concat (map (\v -> [Positive v, Negative v]) (vars clauses))
    ranks = map (how clauses) literals
  in
    reverse (sortOn snd $ zip literals ranks)
    
  
choseLiteral :: Clauses -> Literal
choseLiteral clauses =
    fst (head (rankAll rankMixedLike clauses))

findUnitaryClause :: Clauses -> Maybe Literal
findUnitaryClause (SetOf clauses) =
  let
    unitaryclauses =  [ clause | clause <- clauses, len clause == 1] 
  in
    if null unitaryclauses then Nothing
    else case head unitaryclauses of
      Disjunction [lit] -> Just lit
      _ -> Nothing

    
bindLitTrue :: Literal -> (Variable,Bool)
bindLitTrue (Positive p) = (p,True)
bindLitTrue (Negative p) = (p,False)

bindLitFalse :: Literal -> (Variable,Bool)
bindLitFalse lit = bindLitTrue (opposite lit)

data SolverMode = Propagating | Deciding deriving Show 

data SolverState = State {
  mode :: SolverMode,
  clauses :: Clauses,
  clausesLearnt :: Clauses,
  lastClauseLearnt :: Clause,
  nodesVisited :: Int,
  decisionLevel::Int,
  model::Valuation,
  rankMOMs :: Ranking,
  rankDLIS :: Ranking,
  rankMixed :: Ranking
  }

prettyPrint0 :: SolverState -> String
prettyPrint0 state =
    let
      a = unsafePerformIO $ readIORef alpha
      b = 1-a
      ranks = printf "Ranking MOM's like:\n%s\n" (show (rankMOMs state))
        ++printf "Ranking DLIS like:\n%s\n" (show (rankDLIS state))
        ++printf "Mixed (%f*MOMS +%f*DLIS) ranking :\n%s\n" a b (show (rankMixed state))
      foundModel = case clauses state of
        SetOf [] -> "Found a model\n"
        _ ->  ""
      foundConflict = if (containsEmpty (clauses state)) then
                        "Conflict found\n" else ""
    in
      printf "Mode: %s\n" (show $ mode state)
      ++printf "Clauses: \n%s\n" (show $ clauses state)
      ++printf "Candidate model:\n%s\n" (show $ model state)
      ++ foundModel
      ++ foundConflict
      ++printf "Decision level: %d, nodes visited %d\n\n" (decisionLevel state) (nodesVisited state)
      ++ ranks

prettyPrint1 :: SolverState -> String
prettyPrint1 state =
    let
      a = unsafePerformIO $ readIORef alpha
      b = 1-a
      -- ranks
      ranks = case clauses state of
        SetOf [] -> ""
        cls -> if containsEmpty cls then "" else
          printf "Ranking MOM's like:\n%s\n" (show (rankMOMs state))
          ++printf "Ranking DLIS like:\n%s\n" (show (rankDLIS state))
          ++printf "Mixed (%.2f*MOMS +%.2f*DLIS) ranking :\n%s\n" a b (show (rankMixed state))
      -- string for model
      foundModel = case clauses state of
        SetOf [] -> "Model found\n"
        _ ->  ""
      --  string for Conflict
      foundConflict = if (containsEmpty (clauses state)) then
                        "Conflict found\n" else ""
      lastLiteral=case mode state of
        Deciding -> "Last literal (if any) has been decided.\n"
        Propagating -> "Last literal has been propagated.\n"
    in
      printf "Nodes visited: %d\n" (nodesVisited state)
      ++ lastLiteral
      ++ printf "Decision level: %d\n" (decisionLevel state) 
      ++ printf "Candidate model:\n%s\n" (show $ model state)
      ++ printf "Clauses: \n%s\n" (show $ clauses state)
      ++ foundModel
      ++ foundConflict
      ++ ranks

instance Show SolverState where
  show = prettyPrint1



initialState :: Clauses -> SolverState
initialState clauses  = State {
  mode = Deciding,
  clauses = clauses,
  clausesLearnt = SetOf [],
  lastClauseLearnt = Disjunction [],
  nodesVisited =0,
  decisionLevel=0,
  model=emptyValuation,
  rankMOMs = rankAll rankMOMsLike clauses,
  rankDLIS = rankAll rankDLISLike clauses,
  rankMixed = rankAll rankMixedLike clauses
  }

newState :: SolverState -> SolverMode -> Literal -> Bool -> SolverState
newState  state mode literal b =
  let
    newClauses = case b of
      False -> setLitFalse (clauses state) literal
      True -> setLitTrue  (clauses state) literal
    newModel = case b of
      False -> addPair (bindLitFalse literal) (model state)
      True -> addPair (bindLitTrue literal) (model state)
    newDecisionLevel = case mode of
      Propagating -> decisionLevel state
      Deciding -> decisionLevel state + 1
  in
    State {
    mode = mode,
    clauses = newClauses,
    clausesLearnt = SetOf [],
    lastClauseLearnt = Disjunction [],
    nodesVisited =(nodesVisited state) +1,
    decisionLevel=newDecisionLevel,
    model=newModel,
    rankMOMs = rankAll rankMOMsLike newClauses,
    rankDLIS = rankAll rankDLISLike newClauses,
    rankMixed = rankAll rankMixedLike newClauses
    }


  
dpll :: Clauses -> IO (SolverState,Maybe Valuation)
dpll clauses = dpll0 (initialState clauses)



dpll0 :: SolverState -> IO (SolverState,Maybe Valuation)
dpll0 state = do
  putStrLn (show state)
  flag <- readIORef isInteractive
  if flag then getChar else return 'c'
  dpll1 state

dpll1 :: SolverState -> IO (SolverState,Maybe Valuation)
dpll1  state 
  | clauses state == SetOf [] = return (state,Just (model state))
  | containsEmpty (clauses state) = return (state,Nothing)
  | otherwise =
      case findUnitaryClause (clauses state) of
        Just literal -> dpll0 (newState state Propagating literal True) 
        Nothing ->
          let
            literal = choseLiteral (clauses state)
          in
            do
              ret <- dpll0 (newState state Deciding literal False)
              case ret of
                (oldState,Just valuation) -> return (oldState,Just valuation)
                (oldState,Nothing) ->
                  let
                    newSt = (newState state Deciding  literal True)
                  in
                    dpll0 (newSt {nodesVisited = (nodesVisited oldState) +1 })
   
