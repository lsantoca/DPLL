import Prelude hiding (catch)
import System.Environment
import System.Exit hiding (die)
import System.Random
import Data.IORef
import Control.Exception

--import Quine
import DPLLSolver
import Clauses
import RandomClauses

runOnRandom :: IO ()
runOnRandom = do
  putStrLn "\nRunning DPLL on a random set of clauses\n"
  clauses <- randomIO
  dpll clauses
  return ()

runOnFile :: String -> IO () 
runOnFile path = do
  clauses <- parseFileDIMACS path
  putStrLn "\nRunning DPLL on a DIMACS file containing a set of clauses\n"
  dpll clauses
  return ()
 
main = do
  args <- getArgs
  nameOfFile <- parse args
  case nameOfFile of
    "" -> runOnRandom
    path -> runOnFile path
    
  
parse [] = return ""
parse ["-h"] = usage >> exit
parse ("--nointeractive":xs) = setIsInteractiveFalse >> parse xs 
parse ("--interactive":xs) = setIsInteractiveTrue >> parse xs 
parse ("--alpha":string:xs) = setAlpha string >> parse xs 
parse (fileName:_)   = return fileName
--parse ["-v"] = version >> exit

usage :: IO ()
usage   = putStrLn $
  "Usage: runDPLL [-h] [--nointeractive] [file]\n"
  ++"\trun DPLL algorithm on a set of clauses\n"
  ++"\tfile should be in DIMACS format\n"
  ++"\tif no file is given, then DPLL is run on a random set of clauses"
--version = putStrLn "Haskell tac 0.1"

setIsInteractiveFalse :: IO ()
setIsInteractiveFalse =
  writeIORef isInteractive False
  >>
  putStrLn "Running in batch mode"

setIsInteractiveTrue :: IO ()
setIsInteractiveTrue =
  writeIORef isInteractive True
  >>
  putStrLn "Running in interactive mode"

setAlpha :: String -> IO ()
setAlpha string =
  let
    fail :: SomeException -> IO Float
    fail _ = putStrLn "Could not parse the value for alpha" >> die
  in
  do
    float <- (let x = ((read string)::Float) in x `seq` return x) `catch` fail
    if float < 0 || float > 1 then
      putStrLn ("Value for alpha should be in the interval [0,1]") >> die
      else
      writeIORef alpha float  >>
      writeIORef beta (1 - float) 
    
exit    = exitWith ExitSuccess
die     = exitWith (ExitFailure 1)
