import Data.List
import Text.Printf
  
import System.Environment
import System.Exit

import Quine
import DPLLSolver
import Clauses
import RandomClauses

p i = Positive (Var i)
np i = Negative (Var i)

clause i = Disjunction [np (i+1), p i]
set n = SetOf (map clause [1..n-1] ++ [Disjunction [p n]])


{--
mainOld = do
  clauses <- randomIO
  putStrLn "\nRunning Quine\n"
  (n,_) <- quine clauses
  putStrLn "\nRunning DPLL\n"
  (m,_) <-dpll clauses
  putStrLn (printf "\nQuine: %d nodes, DPLL: %d" n m)
  return ()
--}

