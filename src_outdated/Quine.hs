module Quine where
import Clauses
import Interactions

choseVarFrom :: Clauses -> Variable
choseVarFrom clauses =
  minimum (vars clauses)

quine :: Clauses -> IO (Int,Maybe Valuation)
quine clauses = quine0 clauses emptyValuation 0 

quine0 :: Clauses -> Valuation -> Int -> IO (Int,Maybe Valuation)
quine0 clauses valuation n =
  let
    msg = "\nInput:\n"++show clauses
    msg2 = "\nNodes explored up to now: "++show n
  in
    putStrLn (msg++msg2)
    >>
    quine1 clauses valuation n

quine1 :: Clauses -> Valuation -> Int -> IO (Int,Maybe Valuation)
quine1 (clauses@(SetOf cls)) valuation n
  | null cls =
      let
        msgModel = "Found a model:\n"++show valuation
        msgNodes = "\nNodes explored: "++show n
      in
        waitWithMessage(msgModel ++ msgNodes)
        >> return (n,Just valuation)
  | containsEmpty clauses =
      let
        msg = "The valuation:\n"++show valuation++"\nis not a model"
      in
        waitWithMessage msg
        >>
        return (n,Nothing)
  | otherwise =
      let
        p = choseVarFrom clauses
        firstMsg = "Setting "++show p++" -> False"
      in
        waitWithMessage firstMsg
        >>
        quine0 (setLitFalse clauses (Positive p)) (addPair (p,False) valuation) (n+1)
        >>= (\ret -> case ret of
                (m,Nothing) ->
                  let
                    settingMsg = "Setting "++show p  ++" -> True"
                    secondMsg = "Backtracking to:\n"++show clauses++"\n"++settingMsg
                  in
                  waitWithMessage secondMsg
                  >>
                  quine0 (setLitTrue clauses (Positive p)) (addPair (p,True) valuation) (m+1)
                (m,Just valuation) -> return (m,Just valuation))
