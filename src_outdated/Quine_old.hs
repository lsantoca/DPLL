module Quine where
import Clauses
import Interactions

choseVarFrom :: Clauses -> Variable
choseVarFrom clauses =
  minimum (vars clauses)

quine :: Clauses -> IO (Maybe Valuation)
quine clauses = quine0 clauses emptyValuation 

quine0 :: Clauses -> Valuation -> IO (Maybe Valuation)
quine0 clauses valuation =
  let
    msg = "\nInput:\n"++show clauses
  in
    putStrLn msg
    >>
    quine1 clauses valuation

quine1 :: Clauses -> Valuation -> IO (Maybe Valuation)
quine1 (clauses@(SetOf cls)) valuation
  | null cls =
      let
        msg = "Found a model:\n"++show valuation
      in
        waitWithMessage msg
        >> return (Just valuation)
  | containsEmpty clauses =
      let
        msg = "The valuation:\n"++show valuation++"\nis not a model"
      in
        waitWithMessage msg
        >>
        return Nothing
  | otherwise =
      let
        p = choseVarFrom clauses
        firstMsg = "Setting "++show p++" -> False"
      in
        waitWithMessage firstMsg
        >>
        quine0 (setLitFalse clauses (Positive p)) (addPair (p,False) valuation)
        >>= (\ret -> case ret of
                Nothing ->
                  let
                    settingMsg = "Setting "++show p  ++" -> True"
                    secondMsg = "Backtracking to:\n"++show clauses++"\n"++settingMsg
                  in
                  waitWithMessage secondMsg
                  >>
                  quine0 (setLitTrue clauses (Positive p)) (addPair (p,True) valuation)
                Just valuation -> return (Just valuation))
