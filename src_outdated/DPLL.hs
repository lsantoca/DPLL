module DPLL where
import Data.List
import Text.Printf
import Clauses
import Interactions

alpha = 0.7
beta = 0.3

rankMoin :: Clauses -> Literal -> Float
rankMoin (SetOf clauses) literal =
  sum [1/fromIntegral (len clause) | clause <- clauses, contains clause literal ]

rankPlus (SetOf clauses) literal =
  fromIntegral
      (sum [len clause | clause <- clauses, contains clause (opposite literal)])/
      fromIntegral (sum [len clause | clause <- clauses])
    
rankMoinPlus clauses literal =
  let
    rankmoin = rankMoin clauses literal 
    rankplus = rankPlus  clauses literal 
  in
    alpha * rankmoin + beta * rankplus

rankAll :: (Clauses -> Literal -> Float) -> Clauses -> [(Literal,Float)]
rankAll how clauses =
  let
    literals = concat (map (\v -> [Positive v, Negative v]) (vars clauses))
    ranks = map (how clauses) literals
  in
    sortOn snd $ zip literals ranks
    
  
choseLiteral :: Clauses -> Literal
choseLiteral clauses =
    fst (head (rankAll rankMoinPlus clauses))

findUnitaryClause :: Clauses -> Maybe Literal
findUnitaryClause (SetOf clauses) =
  let
    unitaryclauses =  [ clause | clause <- clauses, len clause == 1] 
  in
    if null unitaryclauses then Nothing
    else case head unitaryclauses of
      Disjunction [lit] -> Just lit
      _ -> Nothing

    
bindLitTrue :: Literal -> (Variable,Bool)
bindLitTrue (Positive p) = (p,True)
bindLitTrue (Negative p) = (p,False)

bindLitFalse :: Literal -> (Variable,Bool)
bindLitFalse lit = bindLitTrue (opposite lit)

dpll :: Clauses -> IO (Int,Maybe Valuation)
dpll clauses = dpll0 clauses emptyValuation 0 

dpll0 :: Clauses -> Valuation -> Int -> IO (Int,Maybe Valuation)
dpll0 clauses valuation n =
  let
    msg = "\nInput:\n"++show clauses
    msg2 = "\nNodes explored up to now: "++show n
  in
    putStrLn (msg++msg2)
    >>
    dpll1 clauses valuation n

dpll1 :: Clauses -> Valuation -> Int -> IO (Int,Maybe Valuation)
dpll1 (clauses@(SetOf cls)) valuation n
  | null cls =
      let
        msgModel = "Found a model:\n"++show valuation
        msgNodes = "\nNodes explored: "++show n
      in
        waitWithMessage(msgModel ++ msgNodes)
        >> return (n,Just valuation)
  | containsEmpty clauses =
      let
        msg = "The valuation:\n"++show valuation++"\nis not a model"
      in
        waitWithMessage msg
        >>
        return (n,Nothing)
  | otherwise =
      case findUnitaryClause clauses of
        Just lit ->
          let
            firstMsg = "Unitary clause found : Setting "++show lit++" -> True"
          in
            waitWithMessage firstMsg
            >>
            dpll0 (setLitTrue clauses lit) (addPair (bindLitTrue lit) valuation) (n+1)
        Nothing ->
          let
            ranksMoin = rankAll rankMoin clauses
            ranksPlus = rankAll rankPlus clauses
            lit = choseLiteral clauses
            msg = printf "\nRank^- : %s\n" (show ranksMoin)
              ++  printf "\nRank^+ : %s\n" (show ranksPlus)
              ++ printf "Setting %s -> False" (show lit)
          in
            waitWithMessage msg
            >>
            dpll0 (setLitFalse clauses lit) (addPair (bindLitFalse lit) valuation) (n+1)
            >>= (\ret -> case ret of
                    (m,Nothing) ->
                      let
                        settingMsg = "Setting "++show lit  ++" -> True"
                        secondMsg = "Backtracking to:\n"++show clauses++"\n"++settingMsg
                      in
                        waitWithMessage secondMsg
                        >>
                        dpll0 (setLitTrue clauses lit) (addPair (bindLitTrue lit) valuation) (m+1)
                    (m,Just valuation) -> return (m,Just valuation))
   
