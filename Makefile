COMMAND=ghc
STATICCOMPOPT=-optl-static -optl-pthread
OPTIONS=
ARCH=${shell uname -p}
OUTFILE=runDPLL_${ARCH}

all:	compile
#	echo ${ARCH}

compile:
	@command -v ${COMMAND} >/dev/null 2>&1 || { echo >&2 "I require ${COMMAND} but it's not installed.  Aborting."; exit 1; }
	cd src/ ; ${COMMAND} ${OPTIONS} -o ${OUTFILE} Main.hs

install:
	cp src/${OUTFILE} bin/${OUTFILE} 

zip: clean
	cd ../ ; make -f DPLL/Makefile zipFromParent
	mv ../DPLL.zip ./
	zip --delete DPLL.zip DPLL/.git/*
	zip --delete DPLL.zip DPLL/src_outdated/*

zipFromParent:
	zip -r DPLL.zip DPLL/

clean:
	rm DPLL.zip src/*.hi src/*.o

installzip: install zip
	cp DPLL.zip ../../html/DOCS/

installhtml: installzip
	cp DPLL.zip ../../html/
	rm -rf ../../html/DPLL/
	cd ../../html/ ; unzip DPLL.zip ; rm DPLL.zip
