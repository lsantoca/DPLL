<?php

require_once 'classes/autoload.php';

function echoHtmlPre($string){
            echo "<html><pre>";
            echo $string;
            echo "</pre></html>";
}

class Program extends AnswerToForm {

    public $form;
    private $dir;
    private $alphas=array();
    
    public function __construct() {
        $this->dir=realpath(__DIR__."/examples");
        
        $files=explode("\n",shell_exec("ls $this->dir/*.cnf"));
        $choices = [['label'=>'tiré au hasard','name' => 'hasard']];
        foreach($files as $file) {
            if($file != ''){
                $name=basename($file);
                array_push($choices,['label'=>"Fichier $name",'name' => $name]);
            }
        }

        $this->alphas=[['label'=>'Défaut : 0.7','name' => 'alpha_0.7']];;
        foreach (range(10,0,1) as $i) {
            $alpha = $i / '10';
            $beta = 1 - $alpha;
            $alphabeta="$alpha/$beta";
            array_push($this->alphas,['label' => $alphabeta,'name'=>"alpha_$alpha"]);
        }
        //print_r($this->alphas);
        
        $this->form = new Form('DPLL', [
            'label' => "Executez l'algorithme DPLL ",
            'submitValue'=> 'Go'
            ]);
        
        $this->form->files = new Input('menu','files', [
            'value'=>'Clauses au hasard',
            'values' => $choices,
            'label'=> 'Choisissez l\'ensemble de clauses'
            ]
        );

        $this->form->alphas = new Input('menu','alphas', [
        'value'=>'0.7',
               'label' => 'Rapport MOMS/DLIS',
            'values' => $this->alphas
            ]
        );
        
//        $this->form->textArea = new Input('textArea', 'textarea', [
//            'label' => 'Mettre ici votre ensemble de clauses',
//            'rows' => 10,
//            'cols' => 60
//        ]);
        $this->form->linkInputs();
    }

    private function executableName(){
        //$arch=php_uname("a");
        $arch=trim(shell_exec('uname -p'));
        //echo "Architecture: $arch\n";
        $executable = realpath(dirname(__DIR__). "/bin/runDPLL_$arch");
        //echo $executable;
        if(!is_executable($executable)){
            echoHtmlPre("$executable is not executable\n");
            exit(0);
        }
        return $executable;
    }

    private function getArg($choice){
        $arg = '';
        if($choice != 'hasard'){
            $arg = "$this->dir/$choice";
        }
        return $arg;
    }
    
        function run2($file,$alpha){
        $arg=$this->getArg($file);
        $executable=$this->executableName();
        $output = shell_exec(trim("$executable --alpha $alpha $arg"));
        echoHtmlPre($output);
    }
    
    function run($string) {        
        $errFile="/tmp/luigi-error-output.txt";
        $descriptorspec = array(
            0 => array("pipe", "r"), // stdin is a pipe that the child will read from
            1 => array("pipe", "w"), // stdout is a pipe that the child will write to
            2 => array("file",$errFile , "w") // stderr is a file to write to
        );
        //$cwd = '/tmp';
        $cwd = NULL;
        $env = array();

        
        //$executable = 'ls';
        /*
        exec($executable,$output,$ret_var);
        echo "Ret var $ret_var\n";
        echoHtmlPre(implode("\n",$output));
        */

        $executable = $this->executableName(); 
        $process = proc_open($executable, $descriptorspec, $pipes, $cwd, $env);

        
        if (is_resource($process)) {
            // $pipes now looks like this:
            // 0 => writeable handle connected to child stdin
            // 1 => readable handle connected to child stdout
            // Any error output will be appended to $errFile

            //fwrite($pipes[0], $string);
            //fclose($pipes[0]);
            $output=stream_get_contents($pipes[1]);
            fclose($pipes[1]);

            // It is important that you close any pipes before calling
            // proc_close in order to avoid a deadlock
            $return_value = proc_close($process);

  
            if($return_value != 0){
               $msg .="Exited with code : $return_value\n"
                    .file_get_contents($errFile);
               $output.=$msg;
            }
            echoHtmlPre($output);
        } else {
            echoHtmlPre("Problem executing $executable");
        }
    
    }

    public function execute() {
        if (!$this->isActive()) {
            return;
        }
        $this->form->fromPost();
        //$this->run($this->form->textArea->value);
        $file=$this->form->files->value;
        //echo $file;
        //print_r($this->form->alphas->value);
        $alpha=substr($this->form->alphas->value,6);
        //echo $alpha;exit;
        $this->run2($this->form->files->value,$alpha);
        exit(0);
    }
}

$program = new Program;
$program->execute();
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="STYLESHEET" type="text/css" href="style.css" />
</head>
    <body>
    <h2>Démos de l'algorithme Davis-Putnam-Longemann-Loveland</h2>
        <?php $program->html(2); ?>
    </body>
</html>